Name:                 powertop
Version:              2.15
Release:              2
Summary:              Power consumption tool for Linux
License:              GPL-2.0-only AND LGPL-2.1-only AND ISC
URL:                  https://github.com/fenrus75/powertop
Source0:              https://github.com/fenrus75/%{name}/archive/v%{version}/%{name}-%{version}.tar.gz
Source1:              powertop.service

Patch1:               backport-powertop-2.7-always-create-params.patch
Patch2:               powertop-2.15-gettext-0.19.patch
Patch6001:            backport-powertop-2.15-fix-configure-to-support-ncurses-w-tinfo.patch
Patch6002:            backport-powertop-2.15-fixed-riscv-t-head-1520-cpu-identification.patch

BuildRequires:        autoconf autoconf-archive automake libtool
BuildRequires:        gcc gcc-c++ systemd
BuildRequires:        gettext-devel
BuildRequires:        pkgconfig(bash-completion)
BuildRequires:        pkgconfig(libnl-3.0) >= 3.0
BuildRequires:        pkgconfig(libnl-genl-3.0) >= 3.0
BuildRequires:        pkgconfig(libpci)
BuildRequires:        pkgconfig(ncursesw)
BuildRequires:        pkgconfig(zlib)
Requires(post):       coreutils
%{?systemd_requires}
Provides:             bundled(kernel-event-lib)

%description
Powertop is a Linux tool to diagnose issues with power consumption and
power management.In addition to being a diagnostic tool, powertop also
has an interactive mode where the user can experiment various power
management settings for cases where the Linux distribution has not enabled these settings.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
# workaroud for 'error: too many loops'
autoreconf -fi || autoreconf -fi
%configure
%make_build

%install
%make_install

install -Dd $RPM_BUILD_ROOT%{_localstatedir}/cache/%{name}
touch $RPM_BUILD_ROOT%{_localstatedir}/cache/%{name}/{saved_parameters.powertop,saved_results.powertop}
%find_lang %{name}

install -Dpm 644 %{SOURCE1} $RPM_BUILD_ROOT%{_unitdir}/powertop.service

%preun
%systemd_preun powertop.service

%postun
%systemd_postun_with_restart powertop.service

%post
%systemd_post powertop.service

touch %{_localstatedir}/cache/powertop/{saved_parameters.powertop,saved_results.powertop} &> /dev/null || :

%files -f %{name}.lang
%license COPYING
%doc README.md README.traceevent CONTRIBUTE.md TODO
%{_sbindir}/powertop
%{_unitdir}/powertop.service
%dir %{_localstatedir}/cache/powertop
%ghost %{_localstatedir}/cache/powertop/saved_parameters.powertop
%ghost %{_localstatedir}/cache/powertop/saved_results.powertop
%{_datadir}/bash-completion/completions/powertop

%files help
%{_mandir}/man8/powertop.8*

%changelog
* Tue Jan 07 2025 Funda Wang <fundawang@yeah.net> - 2.15-2
- fix build with latest gettext
- harden systemd service file from openSUSE
- add patch regarding risc-v arch support

* Sat May 6 2023 panxiaohe <pan_xiaohe@hoperun.com> - 2.15-1
- update to 2.15

* Thu Feb 24 2022 wangchen <wangchen137@h-partners.com> - 2.14-2
- fix compatibility with ncurses-6.3

* Mon Feb 21 2022 panxiaohe <panxh.life@foxmail.com> - 2.14-1
- update to 2.14

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 2.13-2
- DESC: delete -Sgit from %autosetup, and delete BuildRequires git

* Thu Jan 21 2021 yixiangzhike <zhangxingliang3@huawei.com> - 2.13-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update to 2.13

* Sat Sep 19 2020 liquor <lirui130@huawei.com> - 2.9-12
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:modify the service file path to load the service correctly

* Fri Sep 27 2019 chengquan<chengquan3@huawei.com> - 2.9-11
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix spec rule in openeuler

* Fri Sep 06 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.9-10
- Package init
